########################################################################

set default_part xcku040-ffva1156-2-e

# These describe the IP core to be built.
set ip_name vio
set ip_vendor xilinx.com
set ip_library ip
set ip_version 3.0

# This is the name to give to the generated module.
set module_name gtwizard_ultrascale_0_vio_0

# These are the settings to apply to the generated module.
set module_properties { 
  CONFIG.C_PROBE_OUT15_INIT_VAL {0x00040000}
  CONFIG.C_PROBE_OUT15_WIDTH {32}
  CONFIG.C_PROBE_OUT14_WIDTH {7}
  CONFIG.C_PROBE_OUT11_WIDTH {1}
  CONFIG.C_PROBE_OUT10_WIDTH {1}
  CONFIG.C_PROBE_OUT9_WIDTH {4}
  CONFIG.C_PROBE_OUT8_WIDTH {4}
  CONFIG.C_PROBE_OUT7_WIDTH {3}
  CONFIG.C_PROBE_OUT6_WIDTH {1}
  CONFIG.C_PROBE_OUT5_WIDTH {1}
  CONFIG.C_PROBE_OUT4_WIDTH {1}
  CONFIG.C_PROBE_OUT3_WIDTH {1}
  CONFIG.C_PROBE_OUT2_WIDTH {1}
  CONFIG.C_PROBE_OUT1_WIDTH {1}
  CONFIG.C_PROBE_OUT0_WIDTH {1}
  CONFIG.C_PROBE_IN14_WIDTH {32}
  CONFIG.C_PROBE_IN13_WIDTH {7}
  CONFIG.C_PROBE_IN12_WIDTH {1}
  CONFIG.C_PROBE_IN11_WIDTH {1}
  CONFIG.C_PROBE_IN10_WIDTH {1}
  CONFIG.C_PROBE_IN9_WIDTH {1}
  CONFIG.C_PROBE_IN8_WIDTH {1}
  CONFIG.C_PROBE_IN7_WIDTH {1}
  CONFIG.C_PROBE_IN6_WIDTH {1}
  CONFIG.C_PROBE_IN5_WIDTH {1}
  CONFIG.C_PROBE_IN4_WIDTH {1}
  CONFIG.C_PROBE_IN3_WIDTH {4}
  CONFIG.C_PROBE_IN2_WIDTH {1}
  CONFIG.C_PROBE_IN1_WIDTH {1}
  CONFIG.C_PROBE_IN0_WIDTH {1}
  CONFIG.C_NUM_PROBE_OUT {18}
  CONFIG.C_NUM_PROBE_IN {16}
}

# The part to target.
if { $::argc == 0 } {
    set part $default_part
} else {
    set part [lindex $::argv 0]
}

########################################################################

package require vivado_utils

vivado_utils::vivado_create_ip \
    $ip_name $ip_vendor $ip_library $ip_version $module_name $module_properties $part

########################################################################