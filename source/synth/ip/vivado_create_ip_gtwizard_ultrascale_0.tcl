########################################################################

set default_part xcku040-ffva1156-2-e

# These describe the IP core to be built.
set ip_name gtwizard_ultrascale
set ip_vendor xilinx.com
set ip_library ip
set ip_version 1.7

# This is the name to give to the generated module.
set module_name gtwizard_ultrascale_0

# These are the settings to apply to the generated module.
set module_properties { 
  CONFIG.CHANNEL_ENABLE {X0Y11}
  CONFIG.TX_MASTER_CHANNEL {X0Y11}
  CONFIG.RX_MASTER_CHANNEL {X0Y11}
  CONFIG.TX_LINE_RATE {10.24}
  CONFIG.TX_REFCLK_FREQUENCY {320}
  CONFIG.TX_OUTCLK_SOURCE {TXPLLREFCLK_DIV1}
  CONFIG.RX_LINE_RATE {10.24}
  CONFIG.RX_PLL_TYPE {QPLL1}
  CONFIG.RX_REFCLK_FREQUENCY {320}
  CONFIG.RX_BUFFER_MODE {0}
  CONFIG.RX_EQ_MODE {LPM}
  CONFIG.RX_JTOL_FC {6.1427714}
  CONFIG.RX_COMMA_ALIGN_WORD {4}
  CONFIG.RX_COMMA_SHOW_REALIGN_ENABLE {false}
  CONFIG.RX_SLIDE_MODE {PCS}
  CONFIG.ENABLE_OPTIONAL_PORTS {drpaddr_in drpclk_in drpdi_in drpen_in drpwe_in loopback_in rxcdrreset_in rxpolarity_in rxprbscntreset_in rxprbssel_in txpippmen_in txpippmovrden_in txpippmpd_in txpippmsel_in txpippmstepsize_in txpolarity_in txprbsforceerr_in txprbssel_in drpdo_out drprdy_out rxcdrlock_out rxoutclk_out rxoutclkpcs_out rxprbserr_out rxprbslocked_out txbufstatus_out txoutclk_out txoutclkfabric_out txoutclkpcs_out}
  CONFIG.RX_REFCLK_SOURCE {X0Y11 clk0+1}
  CONFIG.TX_REFCLK_SOURCE {X0Y11 clk1}
  CONFIG.RX_RECCLK_OUTPUT {X0Y11 clk0}
  CONFIG.LOCATE_TX_USER_CLOCKING {CORE}
  CONFIG.LOCATE_RX_USER_CLOCKING {CORE}
  CONFIG.TXPROGDIV_FREQ_VAL {320}
  CONFIG.FREERUN_FREQUENCY {125}
} 

# The part to target.
if { $::argc == 0 } {
    set part $default_part
} else {
    set part [lindex $::argv 0]
}

########################################################################

package require vivado_utils

vivado_utils::vivado_create_ip \
    $ip_name $ip_vendor $ip_library $ip_version $module_name $module_properties $part

########################################################################