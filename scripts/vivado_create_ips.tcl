########################################################################
# The Xilinx Vivado EULA does not allow the distribution of IP cores
# or parts thereof. Therefore the IP core information is distributed
# in the form of Tcl scripts.
#
# This Vivado Tcl script finds IP core creation scripts matching a
# fixed file name pattern, and runs all found scripts to generate the
# corresponding IP cores.
#
# Run from the linux shell command line of your cms-tcds2-firmware area:
#   vivado -mode batch -notrace -nolog -nojou -quiet -source scripts/vivado_create_ips.tcl
# or in the Vivado Tcl console:
#   source vivado_create_ips.tcl
# In order to generate all IP cores directly for the correct target FPGA,
# add this to the command line using the '-tclargs' flag. For example:
#   vivado -mode batch -notrace -nolog -nojou -quiet -source scripts/vivado_create_ips.tcl -tclargs xcku15p-ffva1760-2-e
########################################################################

variable script_dir [file dirname [file normalize [info script]]]
set env(TCLLIBPATH) [list $script_dir]
lappend ::auto_path $script_dir

########################################################################

# The IP core generation scripts are are found by name using the
# following pattern.
set pattern "vivado_create_ip_"
set script_name_pattern "$pattern*.tcl"

# This is how we want to call Vivado.
set vivado_cmd {vivado -mode batch -notrace -nolog -nojou -quiet -source}

set sep_line [string repeat "-" 60]

########################################################################

proc glob_recursive {{dir .} {filespec *} {types {b c f l p s}}} {
    set files [glob -nocomplain -types $types -dir $dir -- $filespec]
    foreach x [glob -nocomplain -types {d} -dir $dir -- *] {
        set files [concat $files \
                   [glob_recursive [file join [pwd] $x] $filespec $types]]
    }
    set filelist {}
    foreach x $files {
        lappend filelist [file normalize $x]
    }
    return $filelist;
}

########################################################################

# Find out if a target part is specified.
if { $::argc == 0 } {
    unset -nocomplain part
} else {
    set part [lindex $::argv 0]
}

# Find all the Vivado IP core creation scripts.
set ip_scripts [glob_recursive . $script_name_pattern]
set s_or_not ""
if {[llength $ip_scripts] > 1} {
    set s_or_not "s"
}

set ip_scripts [lsort $ip_scripts]

puts "$sep_line"
puts "Found [llength $ip_scripts] IP core creation script$s_or_not"
puts "$sep_line"

# Now process all found scripts.
foreach {script_name} $ip_scripts {

    puts "Processing [file tail "$script_name"]"
    puts "$sep_line"

    # Derive the Vivado project name from the script name. (NOTE: This
    # relies on a naming convention.)
    set vivado_project_name [file rootname [file tail $script_name]]

    # Derive the expected Vivado project directory name from the
    # project name.
    set dir_name $vivado_project_name

    # Remove any possible left-over Vivado project directory.
    file delete -force -- $dir_name

    # Have Vivado run the script and generate the IP core.
    # exec {*}$vivado_cmd $script_name
    set args ""
    if { [info exists part] } {
        set args " -tclargs $part"
    }
    set status [catch {exec {*}$vivado_cmd $script_name$args} err]
    if {$status != 0} {
        puts "A problem occurred:"
        set tmp {}
        foreach i [split $err "\n"] {
            append tmp "  !!! $i\n"
        }
        puts -nonewline $tmp
        break
    } else {

        # Find the produced IP core name and move the produced IP core
        # to where it should go. (I.e. to where the original script
        # lives.)
        set ip_name [regsub ***=$pattern $vivado_project_name ""]
        set ip_file_name "$ip_name.xci"
        set created_ip_file [glob_recursive $dir_name $ip_file_name]
        set target_base_name [file dirname $script_name]
        set target_sub_name $ip_name
        set target_dir_name [file join $target_base_name $target_sub_name]
        file mkdir $target_dir_name
        file rename -force $created_ip_file $target_dir_name
    }

    # Remove the Vivado project directory.
    file delete -force -- $dir_name
}
puts "$sep_line"

puts "Done"

########################################################################
