########################################################################

namespace eval vivado_utils {
    set version 0.1
}

package provide vivado_utils $vivado_utils::version

########################################################################

proc vivado_utils::vivado_create_ip {ip_name \
                                         ip_vendor \
                                         ip_library ip_version \
                                         module_name \
                                         module_properties \
                                         {part ""} \
                                         {board ""}} {
    set project_name vivado_create_ip_$module_name
    set dir_name $project_name

    set list_projs [get_projects -quiet]
    if { $list_projs eq "" } {
        create_project $project_name $dir_name
        if { $part ne "" } {
            set_property PART $part [current_project]
        }
        if { $board ne "" } {
            set_property BOARD_PART $board [current_project]
        }
        set_property target_language VHDL [current_project]
        set_property simulator_language Mixed [current_project]
    }

    create_ip -name $ip_name \
        -vendor $ip_vendor \
        -library $ip_library \
        -version $ip_version \
        -module_name $module_name

    set_property -dict $module_properties [get_ips $module_name]
}

########################################################################
