Tx phase aligner core reference design

It is recommended to read the reference note file: tx_phase_aligner_reference_note.pdf

## Note about the HPTD IP cores
For licensing reasons it is not possible to distribute the required IP
cores directly as part of this repository. Instead, all required IP
cores are (re)created using Vivado Tcl scripts. After cloning this
repo, please run the following to generate all IP cores.

```
vivado -mode batch -notrace -nolog -nojou -quiet -source scripts/vivado_create_ips.tcl
```